package com.example.android.quizzapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //TextView for start button
    private TextView mTextViewStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Assign the textview
        mTextViewStart = findViewById(R.id.text_start);

        //Assign onclickListener to the textview start
        mTextViewStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Start Activity_Questions
                Intent mQuestionsIntent = new Intent(MainActivity.this, QuestionsActivity.class);
                MainActivity.this.startActivity(mQuestionsIntent);
                finish();
            }
        });

    }
}
