package com.example.android.quizzapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

public class QuestionsActivity extends AppCompatActivity {

    //Question 1 - Buttons
    protected Button mButtonConfirmQuestion1;
    protected RadioButton mRadioButtonQuestion1Answer1;
    protected RadioButton mRadioButtonQuestion1Answer2;
    protected RadioButton mRadioButtonQuestion1Answer3;
    protected RadioButton mRadioButtonQuestion1Answer4;

    //Question 2 - Button and CheckBox
    protected Button mButtonConfirmQuestion2;
    protected CheckBox mCheckBoxQuestion2Answer1;
    protected CheckBox mCheckBoxQuestion2Answer2;
    protected CheckBox mCheckBoxQuestion2Answer3;
    protected CheckBox mCheckBoxQuestion2Answer4;

    //Question 3 - Button and EditText
    protected Button mButtonConfirmQuestion3;
    protected EditText mEditTextQuestion3Answer1;

    //Question 4 - Buttons
    protected Button mButtonConfirmQuestion4;
    protected RadioButton mRadioButtonQuestion4Answer1;
    protected RadioButton mRadioButtonQuestion4Answer2;
    protected RadioButton mRadioButtonQuestion4Answer3;
    protected RadioButton mRadioButtonQuestion4Answer4;

    //Question 5 - Buttons
    protected Button mButtonConfirmQuestion5;
    protected RadioButton mRadioButtonQuestion5Answer1;
    protected RadioButton mRadioButtonQuestion5Answer2;
    protected RadioButton mRadioButtonQuestion5Answer3;
    protected RadioButton mRadioButtonQuestion5Answer4;

    //Question 6 - Buttons
    protected Button mButtonConfirmQuestion6;
    protected RadioButton mRadioButtonQuestion6Answer1;
    protected RadioButton mRadioButtonQuestion6Answer2;
    protected RadioButton mRadioButtonQuestion6Answer3;
    protected RadioButton mRadioButtonQuestion6Answer4;

    //Correct answers logos
    protected ImageView mImageCorrectAnswerQuestion1;
    protected ImageView mImageCorrectAnswerQuestion2;
    protected ImageView mImageCorrectAnswerQuestion3;
    protected ImageView mImageCorrectAnswerQuestion4;
    protected ImageView mImageCorrectAnswerQuestion5;
    protected ImageView mImageCorrectAnswerQuestion6;

    //Finish and reset Buttons
    protected Button mButtonFinish;
    protected Button mButtonReset;

    //Toast for messages
    protected Toast mToast;

    //Layouts of the questions
    protected LinearLayout mLinearLayoutQuestion1;
    protected LinearLayout mLinearLayoutQuestion2;
    protected LinearLayout mLinearLayoutQuestion3;
    protected LinearLayout mLinearLayoutQuestion4;
    protected LinearLayout mLinearLayoutQuestion5;
    protected LinearLayout mLinearLayoutQuestion6;

    //Int to Count Question number and Ints to score the correct answers
    protected int mQuestionNumber;
    protected int mCorrectAnswers;

    //Int to know what question is correct, to show the correct logo at the end (0 = incorrect, 1 = correct)
    protected int mQuestion1;
    protected int mQuestion2;
    protected int mQuestion3;
    protected int mQuestion4;
    protected int mQuestion5;
    protected int mQuestion6;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);

        //Assign Question 1 Buttons
        mButtonConfirmQuestion1 = findViewById(R.id.button_question_1_confirm);
        mRadioButtonQuestion1Answer1 = findViewById(R.id.radio_button_question_1_answer1);
        mRadioButtonQuestion1Answer2 = findViewById(R.id.radio_button_question_1_answer2);
        mRadioButtonQuestion1Answer3 = findViewById(R.id.radio_button_question_1_answer3);
        mRadioButtonQuestion1Answer4 = findViewById(R.id.radio_button_question_1_answer4);

        //Assign Question 2 Buttons and CheckBoxes
        mButtonConfirmQuestion2 = findViewById(R.id.button_question_2_confirm);
        mCheckBoxQuestion2Answer1 = findViewById(R.id.checkbox_question_2_answer1);
        mCheckBoxQuestion2Answer2 = findViewById(R.id.checkbox_question_2_answer2);
        mCheckBoxQuestion2Answer3 = findViewById(R.id.checkbox_question_2_answer3);
        mCheckBoxQuestion2Answer4 = findViewById(R.id.checkbox_question_2_answer4);

        //Assign Question 3 Button and EditText
        mButtonConfirmQuestion3 = findViewById(R.id.button_question_3_confirm);
        mEditTextQuestion3Answer1 = findViewById(R.id.edittext_question_3_answer1);

        //Assign Question 4 Buttons
        mButtonConfirmQuestion4 = findViewById(R.id.button_question_4_confirm);
        mRadioButtonQuestion4Answer1 = findViewById(R.id.radio_button_question_4_answer1);
        mRadioButtonQuestion4Answer2 = findViewById(R.id.radio_button_question_4_answer2);
        mRadioButtonQuestion4Answer3 = findViewById(R.id.radio_button_question_4_answer3);
        mRadioButtonQuestion4Answer4 = findViewById(R.id.radio_button_question_4_answer4);

        //Assign Question 5 Buttons
        mButtonConfirmQuestion5 = findViewById(R.id.button_question_5_confirm);
        mRadioButtonQuestion5Answer1 = findViewById(R.id.radio_button_question_5_answer1);
        mRadioButtonQuestion5Answer2 = findViewById(R.id.radio_button_question_5_answer2);
        mRadioButtonQuestion5Answer3 = findViewById(R.id.radio_button_question_5_answer3);
        mRadioButtonQuestion5Answer4 = findViewById(R.id.radio_button_question_5_answer4);

        //Assign Question 6 Buttons
        mButtonConfirmQuestion6 = findViewById(R.id.button_question_6_confirm);
        mRadioButtonQuestion6Answer1 = findViewById(R.id.radio_button_question_6_answer1);
        mRadioButtonQuestion6Answer2 = findViewById(R.id.radio_button_question_6_answer2);
        mRadioButtonQuestion6Answer3 = findViewById(R.id.radio_button_question_6_answer3);
        mRadioButtonQuestion6Answer4 = findViewById(R.id.radio_button_question_6_answer4);

        //Assign correct question images
        mImageCorrectAnswerQuestion1 = findViewById(R.id.image_question_1_correct_answer);
        mImageCorrectAnswerQuestion2 = findViewById(R.id.image_question_2_correct_answer);
        mImageCorrectAnswerQuestion3 = findViewById(R.id.image_question_3_correct_answer);
        mImageCorrectAnswerQuestion4 = findViewById(R.id.image_question_4_correct_answer);
        mImageCorrectAnswerQuestion5 = findViewById(R.id.image_question_5_correct_answer);
        mImageCorrectAnswerQuestion6 = findViewById(R.id.image_question_6_correct_answer);

        //Assign Finish and Reset Buttons
        mButtonFinish = findViewById(R.id.button_finish);
        mButtonReset = findViewById(R.id.button_reset);

        //Assign the LinearLayouts
        mLinearLayoutQuestion1 = findViewById(R.id.layout_question_1);
        mLinearLayoutQuestion2 = findViewById(R.id.layout_question_2);
        mLinearLayoutQuestion3 = findViewById(R.id.layout_question_3);
        mLinearLayoutQuestion4 = findViewById(R.id.layout_question_4);
        mLinearLayoutQuestion5 = findViewById(R.id.layout_question_5);
        mLinearLayoutQuestion6 = findViewById(R.id.layout_question_6);

        //Assign onclicklistener to the Question 1 Confirm Button
        mButtonConfirmQuestion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //if & else to check if the player answer a question before continue to the next question - if not a toast message will appear
                if (mRadioButtonQuestion1Answer1.isChecked() || mRadioButtonQuestion1Answer2.isChecked() || mRadioButtonQuestion1Answer3.isChecked() || mRadioButtonQuestion1Answer4.isChecked()) {
                    confirmQuestion();
                } else {
                    needAnswerQuestion();
                }
            }
        });

        //Assign onclicklistener to the Question 2 Confirm Button
        mButtonConfirmQuestion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //if & else to check if the player answer a question before continue to the next question - if not a toast message will appear
                if (mCheckBoxQuestion2Answer1.isChecked() || mCheckBoxQuestion2Answer2.isChecked() || mCheckBoxQuestion2Answer3.isChecked() || mCheckBoxQuestion2Answer4.isChecked()) {
                    confirmQuestion();
                } else {
                    needAnswerQuestion();
                }
            }
        });

        //Assign onclicklistener to the Question 3 Confirm Button
        mButtonConfirmQuestion3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Strings to check the edittext and the answer to execute the if & else statements
                String checkEdit = mEditTextQuestion3Answer1.getText().toString();
                String checkAnswer = getString(R.string.answer);

                //if & else to check if the player answer a question before continue to the next question - if not a toast message will appear
                if (checkEdit.equalsIgnoreCase(checkAnswer)) {
                    needAnswerQuestion();
                } else {
                    confirmQuestion();
                }
            }
        });

        //Assign onclicklistener to the Question 4 Confirm Button
        mButtonConfirmQuestion4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //if & else to check if the player answer a question before continue to the next question - if not a toast message will appear
                if (mRadioButtonQuestion4Answer1.isChecked() || mRadioButtonQuestion4Answer2.isChecked() || mRadioButtonQuestion4Answer3.isChecked() || mRadioButtonQuestion4Answer4.isChecked()) {
                    confirmQuestion();
                } else {
                    needAnswerQuestion();
                }
            }
        });

        //Assign onclicklistener to the Question 5 Confirm Button
        mButtonConfirmQuestion5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //if & else to check if the player answer a question before continue to the next question - if not a toast message will appear
                if (mRadioButtonQuestion5Answer1.isChecked() || mRadioButtonQuestion5Answer2.isChecked() || mRadioButtonQuestion5Answer3.isChecked() || mRadioButtonQuestion5Answer4.isChecked()) {
                    confirmQuestion();
                } else {
                    needAnswerQuestion();
                }
            }
        });

        //Assign onclicklistener to the Question 6 Confirm Button
        mButtonConfirmQuestion6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //if & else to check if the player answer a question before continue to the next question - if not a toast message will appear
                if (mRadioButtonQuestion6Answer1.isChecked() || mRadioButtonQuestion6Answer2.isChecked() || mRadioButtonQuestion6Answer3.isChecked() || mRadioButtonQuestion6Answer4.isChecked()) {
                    confirmQuestion();
                } else {
                    needAnswerQuestion();
                }
            }
        });

        //Assign onclickListener to the reset button
        mButtonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Go to the main layout if the reset button is pressed
                Intent mQuestionsIntent = new Intent(QuestionsActivity.this, MainActivity.class);
                QuestionsActivity.this.startActivity(mQuestionsIntent);
                finish();
            }
        });

        //Assign onclickListener to the finish button
        mButtonFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Call method with the score obtained
                score();
                //Call method to show correct logos
                correctQuestions();
                //Call method to switch finish button
                switchFinishButton();
            }
        });

    }

    //Method of the confirm buttons for each question
    public void confirmQuestion() {

        switch (mQuestionNumber) {

            //Question 1 method
            case 0:

                //Set the visibility of the button and the layout of the current and next question
                mButtonConfirmQuestion1.setVisibility(View.GONE);
                mLinearLayoutQuestion1.setVisibility(View.GONE);
                mButtonConfirmQuestion2.setVisibility(View.VISIBLE);
                mLinearLayoutQuestion2.setVisibility(View.VISIBLE);

                //Check if the correct answer is pressed (number 3) to score or not
                if (mRadioButtonQuestion1Answer3.isChecked()) {
                    mQuestion1 = 1;
                    mCorrectAnswers++;
                }
                //Upgrade the Question number
                mQuestionNumber++;
                break;

            //Question 2 method
            case 1:

                //Set the visibility of the button and the layout of the current and next question
                mButtonConfirmQuestion2.setVisibility(View.GONE);
                mLinearLayoutQuestion2.setVisibility(View.GONE);
                mButtonConfirmQuestion3.setVisibility(View.VISIBLE);
                mLinearLayoutQuestion3.setVisibility(View.VISIBLE);

                //Check if the correct answers are selected (number 1 and 4) to score or not
                if (mCheckBoxQuestion2Answer1.isChecked() && mCheckBoxQuestion2Answer4.isChecked()) {

                    if (mCheckBoxQuestion2Answer2.isChecked() || mCheckBoxQuestion2Answer3.isChecked()) {
                        mQuestionNumber++;
                        break;
                    } else {
                        mQuestion2 = 1;
                        mCorrectAnswers++;
                    }
                }
                //Upgrade the Question number
                mQuestionNumber++;
                break;

            //Question 3 method
            case 2:

                //Strings to check the edittext and the answer
                String checkEdit = mEditTextQuestion3Answer1.getText().toString();
                String checkAnswer = getString(R.string.question_3_answer);

                //Set the visibility of the button and the layout of the current and next question
                mButtonConfirmQuestion3.setVisibility(View.GONE);
                mLinearLayoutQuestion3.setVisibility(View.GONE);
                mButtonConfirmQuestion4.setVisibility(View.VISIBLE);
                mLinearLayoutQuestion4.setVisibility(View.VISIBLE);

                //Check if the correct answer is written
                if (checkEdit.equalsIgnoreCase(checkAnswer)) {
                    mQuestion3 = 1;
                    mCorrectAnswers++;
                }

                //Upgrade the Question number
                mQuestionNumber++;
                break;

            //Question 4 method
            case 3:

                //Set the visibility of the button and the layout of the current and next question
                mButtonConfirmQuestion4.setVisibility(View.GONE);
                mLinearLayoutQuestion4.setVisibility(View.GONE);
                mButtonConfirmQuestion5.setVisibility(View.VISIBLE);
                mLinearLayoutQuestion5.setVisibility(View.VISIBLE);

                //Check if the correct answer is selected
                if (mRadioButtonQuestion4Answer2.isChecked()) {
                    mQuestion4 = 1;
                    mCorrectAnswers++;
                }
                //Upgrade the Question number
                mQuestionNumber++;
                break;

            //Question 5 method
            case 4:

                //Set the visibility of the button and the layout of the current and next question
                mButtonConfirmQuestion5.setVisibility(View.GONE);
                mLinearLayoutQuestion5.setVisibility(View.GONE);
                mButtonConfirmQuestion6.setVisibility(View.VISIBLE);
                mLinearLayoutQuestion6.setVisibility(View.VISIBLE);

                //Check if the correct answer is selected
                if (mRadioButtonQuestion5Answer4.isChecked()) {
                    mQuestion5 = 1;
                    mCorrectAnswers++;
                }
                //Upgrade the Question number
                mQuestionNumber++;
                break;

            //Question 6 method
            case 5:

                //Set the visibility of the button and the layout of the current and next question
                mButtonConfirmQuestion6.setVisibility(View.GONE);
                mLinearLayoutQuestion6.setVisibility(View.GONE);

                //Check if the correct answer is selected
                if (mRadioButtonQuestion6Answer3.isChecked()) {
                    mQuestion6 = 1;
                    mCorrectAnswers++;
                }
                //Call finishgame method
                switchResetButton();
                break;

        }

    }

    //Method to ask for an answer to the player
    public void needAnswerQuestion() {

        //Shows a toast saying that the player must answer before continue
        mToast.makeText(this, "You need to answer before continue!", Toast.LENGTH_SHORT).show();
    }

    //Method to switch the reset button with the finish button
    public void switchResetButton() {

        //Dissapear reset button
        mButtonReset.setVisibility(View.GONE);
        //Appear finish button
        mButtonFinish.setVisibility(View.VISIBLE);
    }

    //Method to switch the reset button with the finish button
    public void switchFinishButton() {

        //Dissapear finish button
        mButtonFinish.setVisibility(View.GONE);
        //Appear reset button
        mButtonReset.setVisibility(View.VISIBLE);

    }

    //Method to show the score
    public void score() {

        //Shows a toast saying that the player must answer before continue
        if (mCorrectAnswers > 0) {
            if (mCorrectAnswers == 6) {
                mToast.makeText(this, "Wow! You asnwer correctly all questions!", Toast.LENGTH_SHORT).show();
            } else {
                mToast.makeText(this, "You answer correctly " + mCorrectAnswers + "/6 questions!", Toast.LENGTH_SHORT).show();
            }
        } else {
            mToast.makeText(this, "You failed all questions! :(", Toast.LENGTH_SHORT).show();
        }
    }

    //Method to show correct logos for each correct question
    public void correctQuestions() {

        //Show the correct logo if the answer was correct
        if (mQuestion1 == 1) {
            mImageCorrectAnswerQuestion1.setVisibility(View.VISIBLE);
        }
        if (mQuestion2 == 1) {
            mImageCorrectAnswerQuestion2.setVisibility(View.VISIBLE);
        }
        if (mQuestion3 == 1) {
            mImageCorrectAnswerQuestion3.setVisibility(View.VISIBLE);
        }
        if (mQuestion4 == 1) {
            mImageCorrectAnswerQuestion4.setVisibility(View.VISIBLE);
        }
        if (mQuestion5 == 1) {
            mImageCorrectAnswerQuestion5.setVisibility(View.VISIBLE);
        }
        if (mQuestion6 == 1) {
            mImageCorrectAnswerQuestion6.setVisibility(View.VISIBLE);
        }
    }

}
